# -*- coding: utf-8 -*-
# @Time    : 2021/8/4 14:53
# @Author  : 万方名
# @FileName: views.py

from django.http import HttpResponse
from django.shortcuts import render


def hello(request):
    return HttpResponse('Hello world！')


def runnob(request):
    views_name = '你奇怪啥？'
    views_dict = {'name': '菜鸟'}
    return render(request, 'runnoob.html', {'views_dict': views_dict})


def page(request):
    name = 1
    return render(request, 'ifelse.html', {'num': name})
